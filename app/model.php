<?php
	class model {
		public function fetchAll($sql){
			global $con;
			$res = mysqli_query($con,$sql);
			$arr = array();
			while($row = mysqli_fetch_array($res)){
				$arr[] = $row;
			}
			return $arr;
		}
		public function selectAll($table){
			global $con;
			$arr = array();
			$sql = "select * from $table";
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_array($result)){
				$arr[] = $row;
			}
			return $arr;
		}
		public function selectOne($sql){
			global $con;
			$arr = array();
			$result = mysqli_query($con,$sql);
			$arr = mysqli_fetch_array($result);
			return $arr;
		}
		public function execute($sql){
			global $con;
			mysqli_query($con,$sql);
		}
		public function deleteOne($table,$id){
			global $con;
			$sql = "delete from $table where id = $id";
			mysqli_query($con,$sql);
		}
	} 
?>