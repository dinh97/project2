<div class="row">
	<div class="col-md-9">
		<div class="panel panel-primary">
			<div class="panel-heading"><?php echo $type['name']; ?></div>
			<div class="panel-body book">
				<?php foreach ($book as $row) { ?>
				<div class="col-xs-6 col-md-3 col-sm-3">
					<a href="index.php?controller=view&id=<?php echo $row['id']; ?>" class="thumbnail">
						<img src="public/img/<?php echo $row['image']; ?>" alt="<?php echo $row['name_book']; ?>">
					</a>
					<h5 class="text-center">
						<a href="<?php echo $row['id']; ?>"><?php echo $row['name_book']; ?></a>
					</h5>
				</div>

				<?php } ?>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="panel panel-primary sidebar">
			<div class="panel-heading">Thể loại sách</div>
			<ul class="">
				<?php include "controller/controller_sidebar.php";  ?>
			</ul>
		</div>
	</div>
</div>