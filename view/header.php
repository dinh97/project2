<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8-unicode-ci">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sách Hay Tâm Lý - Kỹ Năng Sống | Ebook Miễn Phí</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/home.css">

</head>
<body>

	<header class="container">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="../tusach">SachVui.Com</a>
				</div>

				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav">
						<li><a href="../tusach">Trang Chủ</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Thể loại sách<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<?php foreach($menu as $row) { ?>
								<li>
									<a href="index.php?controller=book&id=<?php echo $row['id']; ?>">
										<span><?php echo $row['name']; ?></span>
									</a>
								</li>
								<?php } ?>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Thông tin<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="#">Lời nói đầu</a></li>
								<li><a href="#">Bản quyền</a></li>
								<li><a href="#">Liên hệ</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Facebook Fanpage</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Báo cáo vi phạm</a></li>
							</ul>
						</li>
					</ul>

					<form class="navbar-form navbar-right" role="search">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search">
						</div>
						<button type="submit" class="btn btn-default">Search</button>
					</form>
				</div><!-- /.navbar-collapse -->
			</div>
		</nav>
	</header>

	<div class="container">
		<?php
			if(file_exists($controller)==true){
				include $controller;
			} 
		?>
	</div>
	<footer class="container">
		<div >
			<p>&copy 2017 Sachvui.Com, Nonprofit Organization.</p>
		</div>
	</footer>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
