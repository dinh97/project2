<div class="panel panel-default">
<div class="panel-body">
	<div class="col-md-4">
		<img src="public/img/<?php echo $result['image']; ?>" alt="<?php echo $result['name_book']; ?>" class="img-thumbnail cover">
	</div>
	<div class="col-md-8">
		<a href="index.php?controller=view&id=<?php echo $result['id']; ?>">
			<h1><?php echo $result['name_book']; ?></h1>
		</a>
		<h4>Tác giả : <?php echo $result['author']; ?></h4>
		<h4>Thể loại :
			<a href="index.php?controller=book&id=<?php echo $result['id_type']; ?>"><?php echo $result['name']; ?></a> 
		</h4>
		<div class="alert alert-info" role="alert">
			<strong class="fa fa-download"></strong> Vui lòng chọn định dạng file để tải hoặc đọc online.
		</div>
		<a href="" class="btn btn-primary">EPUB</a>
		<a href="" class="btn btn-danger">PDF</a>
		<a href="" class="btn btn-warning">Đọc Online</a>
	</div>	
	<div class="gioi-thieu-sach text-justify">
		<p><?php echo $result['description']; ?></p>
	</div>
</div>
</div>