<?php
	session_start();
	include "../app/config.php";
	include "../app/model.php"; 
	include "../app/controller.php";
	$c = (isset($_GET["controller"])) ? $_GET["controller"] : "";
	$controller = "";
	if($c != "") $controller = "controller/controller_$c.php";
	include "view/home.php";
?>