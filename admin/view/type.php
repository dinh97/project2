<h1>Danh sách thể loại sách</h1>

<a href="index.php?controller=type&action=add_edit" class="btn btn-primary add">Thêm loại ebook mới</a>
<table class="table table-bordered table-striped" id="type">
	<thead class="alert alert-success">
		<tr>
			<th>ID</th>
			<th>Thể loại</th>
			<th>Sửa</th>
			<th>Xóa</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($arr as $row) { ?>
		<tr>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['name']; ?></td>
			<td><a href="index.php?controller=type&action=add_edit&id=<?php echo $row['id']; ?>" class="btn btn-primary">Sửa</a></td>
			<td><a href="index.php?controller=type&action=delete&id=<?php echo $row['id']; ?>" class="btn btn-danger" onclick="return window.confirm('Are you sure?');">Xóa</a></td>
		</tr>
		<?php } ?>
	</tbody>
</table>