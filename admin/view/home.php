
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>Starter Template for Bootstrap</title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="../public/css/admin.css">
    <link rel="stylesheet" type="text/css" href="../public/css/login.css">

</head>

<body>
	<header>
		<div class="container">
			<nav class="navbar navbar-light bg-faded">
       <a class="navbar-brand" href="../">Trang chủ</a>
       <ul class="nav nav-tabs">
        <?php if(isset($_SESSION['user'])){ ?>
        <li class="nav-item pull-right">
         <a href="index.php?controller=login&action=logout" class="nav-link fa fa-sign-out"> Log out</a>
       </li>
       <?php } ?>
       <li class="nav-item pull-right">
         <a href="index.php?controller=login">
           <?php 
            if(isset($_SESSION['user'])) echo '<i class="fa fa-user"></i> '.$_SESSION['user'];
            else echo "Đăng nhập";
           ?>
         </a>
       </li>
     </ul>
   </nav>
 </div>
</header>
<div class="container">
  <?php if(isset($_SESSION['user'])){ ?>
 <div class="menu">
  <ul>
    <li><a href="index.php?controller=type">Thể loại sách</a></li>
    <li><a href="index.php?controller=book">Sách</a></li>
    <li><a href="index.php?controller=admin">User</a></li>
  </ul>
</div>
<?php } ?>
<div class="show">
  <?php
  if (file_exists($controller)) {
    include $controller;
  } 
  ?>
</div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
<script src="../public/js/admin.js"></script>
</body>
</html>
