<fieldset>
	<form class="form-horizontal" action="index.php?controller=book&action=<?php if(isset($id)) echo "edit&id=$id"; else echo "add"; ?>"  method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="name" class="col-sm-3 control-label">Tên sách</label>
		<div class="col-sm-9">
			<?php if(!isset($result['name_book'])){ ?>
			<input type="text" class="form-control" name="name" placeholder="Tên sách">
			<?php } else{ ?>
			<input type="text" class="form-control" name="name" value="<?php echo $result['name_book']; ?>">
			<?php } ?>
		</div>
	</div>
	<div class="form-group">
		<label for="author" class="col-sm-3 control-label">Tác giả</label>
		<div class="col-sm-9 ">
			<input type="text" class="form-control" name="author" placeholder="Tác giả" value="<?php if(isset($result['author'])) echo $result['author']; ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-3 control-label">Thể loại sách</label>
		<div class="col-sm-9">
			<select name="type" class="form-control">
				<?php foreach ($type as $row) { ?>
				<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="image" class="col-sm-3 control-label">Ảnh</label>
		<div class="col-sm-9">
			<input type="file" name="image" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label for="epub" class="col-sm-3 control-label">Link EPUB</label>
		<div class="col-sm-9">
			<input type="text" name="epub" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label for="pdf" class="col-sm-3 control-label">Link PDF</label>
		<div class="col-sm-9">
			<input type="text" name="pdf" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label for="des" class="col-sm-3 control-label">Nội dung tóm tắt</label>
		<div class="col-sm-9">
			<input type="text" name="des" class="form-control" value="<?php if (isset($result['description'])) echo $result['description']; ?>">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="submit" class="btn btn-primary" name="sub">Lưu</button>
		</div>
	</div>
</form>
</fieldset>