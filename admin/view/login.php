
<fieldset>
	<form class="form-horizontal" action="index.php?controller=login&action=login"  method="post">
	<div class="form-group">
		<legend>Đăng nhập</legend>
	</div>
	<div class="form-group">
		<label for="user" class="col-sm-2 control-label">Username</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="user" placeholder="Username">
		</div>
	</div>
	<div class="form-group">
		<label for="pass" class="col-sm-2 control-label">Password</label>
		<div class="col-sm-10">
			<input type="password" class="form-control" name="pass" placeholder="Password">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-primary" name="sub">Sign in</button>
		</div>
	</div>
</form>
</fieldset>