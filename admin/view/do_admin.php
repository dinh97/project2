
<fieldset>
	<form class="form-horizontal" action="index.php?controller=admin&action=<?php if(isset($result['name_book'])) echo "edit&id=$id"; else echo "add"; ?>"  method="post">
	<div class="form-group">
		<label for="user" class="col-sm-2 control-label">Username</label>
		<div class="col-sm-10">
			<?php if(!isset($result['name_book'])){ ?>
			<input type="text" class="form-control" name="user" placeholder="Username">
			<?php } else{ ?>
			<p class="form-control-static"><?php echo $result['name_book']; ?></p>
			<?php } ?>
		</div>
	</div>
	<div class="form-group">
		<label for="pass" class="col-sm-2 control-label">Password</label>
		<div class="col-sm-10 ">
			<input type="password" class="form-control" name="pass" placeholder="Password">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-primary" name="sub">Lưu</button>
		</div>
	</div>
</form>
</fieldset>