<h1>Danh sách sách</h1>

<a href="index.php?controller=book&action=add_edit" class="btn btn-primary add">Thêm sách mới</a>
<table class="table table-bordered table-striped" id="book">
	<thead class="alert alert-success">
		<tr>
			<th>ID</th>
			<th>Tên Sách</th>
			<th>Tác Giả</th>
			<th>Thể Loại</th>
			<th>Ảnh</th>
			<th>Miêu tả</th>
			<th>Link EPUB</th>
			<th>Link PDF</th>
			<th>Sửa</th>
			<th>Xóa</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($arr as $row) { ?>
		<tr>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['name_book']; ?></td>
			<td><?php echo $row['author']; ?></td>
			<td><?php echo $row['name']; ?></td>
			<td><img src="<?php echo "../public/img/".$row['image']; ?>" style="width: 100px;" class="thumbnail"></td>
			<td><?php echo $row['description']; ?></td>
			<td><?php echo $row['epub']; ?></td>
			<td><?php echo $row['pdf']; ?></td>
			<td><a href="index.php?controller=book&action=add_edit&id=<?php echo $row['id']; ?>" class="btn btn-primary">Sửa</a></td>
			<td><a href="index.php?controller=book&action=delete&id=<?php echo $row['id']; ?>" class="btn btn-danger" onclick="return window.confirm('Are you sure?');">Xóa</a></td>
		</tr>
		<?php } ?>
	</tbody>
</table>