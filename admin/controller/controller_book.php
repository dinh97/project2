<?php
	class ControllerBook extends controller{
		public function __construct(){
			parent::__construct();
			$action = isset($_GET['action']) ? $_GET['action']:"show";
			$sql = "select DISTINCT type.name,book.* from book,type where book.id_type = type.id";
			$arr = $this->model->fetchAll($sql);
			$type = $this->model->selectAll('type');
			switch ($action) {
				case 'show':{
					include 'view/book.php';
					break;
				}
				case 'delete':{
					if(isset($_GET['id'])){
						$id = $_GET['id'];
						$this->model->deleteOne("book",$id);
						header("location: index.php?controller=book");
					}
					break;
				}
				case 'add_edit':{
					if(isset($_GET['id'])){
						$id = $_GET['id'];
						$sql = "select * from book where id = $id";
						$result = $this->model->selectOne($sql);
						echo "<h1>Sửa thông tin</h1>";
						include "view/do_book.php";
					}
					else {
						echo "<h1>Thêm loại sách mới</h1>";
						include "view/do_book.php";
					}
					
					break;
				}
				case 'add':{
					if(isset($_POST['sub'])){
						$name = $_POST['name'];
						$author = $_POST['author'];
						$type = $_POST['type'];
						if(isset($_FILES['image'])){
							if($_FILES['image']['size'] > 1048576) die("File quá lớn");
							else{
								move_uploaded_file($_FILES['image']['tmp_name'] ,"../public/img/".$_FILES['image']['name']);
							}
						}
						$image = $_FILES['image']['name'];
						$epub = $_POST['epub'];
						$pdf = $_POST['pdf'];
						$des = $_POST['des'];
						$sql = "insert into book(name_book,author,id_type,image,description,epub,pdf) values ('$name','$author','$type','$image','$des','$epub','$pdf')";
						$this->model->execute($sql);
						header("location: index.php?controller=book");
					}
					break;
				}
				case 'edit':{
					if(isset($_POST['sub'])){
						$name = $_POST['name'];
						$author = $_POST['author'];
						$type = $_POST['type'];
						if(isset($_FILES['image'])){
							if($_FILES['image']['size'] > 1048576) die("File quá lớn");
							else{
								move_uploaded_file($_FILES['image']['tmp_name'] ,"../public/img/".$_FILES['image']['name']);
							}
							$image = $_FILES['image']['name'];
						}
						else $image = $arr[''];
						$epub = $_POST['epub'];
						$pdf = $_POST['pdf'];
						$des = $_POST['des'];
						$id = $_GET['id'];
						$sql = "update book set name_book='$name', author='$author',id_type='$type',image='$image',description='$des',epub='$epub',pdf='$pdf' where id = $id";
						$this->model->execute($sql);
						header("location: index.php?controller=book");
					}
					break;
				}
				default:
					break;
			}
		}
	} 
	new ControllerBook();
 ?>