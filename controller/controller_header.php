<?php
	class ControllerHeader extends controller{
		public function __construct(){
			parent::__construct();
			$menu = $this->model->selectAll('type');
			$c = (isset($_GET["controller"])) ? $_GET["controller"] : "home";
			$controller = "";
			if($c != "") $controller = "controller/controller_$c.php";
			include "view/header.php";
		}
	}
	new ControllerHeader();
 ?>